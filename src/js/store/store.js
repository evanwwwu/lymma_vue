import Vue from 'vue'
import Vuex from 'vuex';
Vue.use(Vuex);

const state = {
    isLogin: false,
    menuOpen: false,
    headShow: true,
    isPop: false
};
/**
    computed:{
        ...Vuex.mapGetters(['showLoading','isLogin','userName'])
    },
 */
const getters = {
    isLogin: state => state.isLogin,
    menuOpen: state => state.menuOpen,
    headShow: state => state.headShow,
    isPop: state => state.isPop
};

// vue 裡用 this.$store.commit('showLoading' , true)
// mutation 必須是同步函數, 很重要
const mutations = {
    isLogin(state, value) {
        state.isLogin = value;  
    },
    menuToggle(state, value) {
        if (value == undefined) {
            state.menuOpen = !state.menuOpen;
        }
        else {
            state.menuOpen = value;
        }
    },
    headShow(state, value) {
        state.headShow = value;
    },
    isPop(state, value) {
        state.isPop = value;
    }
};

const actions = {
    isLogin({ commit},value) {
        commit('isLogin', value);
    },
    login({commit,state}, {email,password}) {
        return new Promise(resolve => {
            commit('showLoading', true);
            console.log('action login', email, password);
            setTimeout(async() => {
                var response = await axios.get('api.txt');
                var data = response.data;
                if (data.status == 'ok') {
                    commit('userName', data.name);
                    // action 不應該直接修改 state 的值, 
                    // 要使用 commit 的方式呼叫 mutations 去改值
                    // 以下寫法在嚴格模式會發生錯誤
                }
                resolve(data);
                commit('showLoading', false);
            }, 1000);
        })
    }

};


// https://vuex.vuejs.org/en/plugins.html
// Plugins
const myPlugin = store => {
    // called when the store is initialized
    store.subscribe((mutation, state) => {
        // called after every mutation.
        // console.log( mutation );
        // The mutation comes in the format of { type, payload }.
    });
};

export default new Vuex.Store({
    plugins: [myPlugin],
    state,
    getters,
    actions,
    mutations,
    strict: true, //嚴格模式
});